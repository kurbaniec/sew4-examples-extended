# Theorie
Beinhaltet die grundsätzliche Theorie zu Nebenläufigkeit und Interprozesskommunikation

# Prozesse vs Threads
Ein Prozess ist ein Programm bei seiner Ausführung. Zum Zeitpunkt der Ausführung erhält der Prozess somit die volle Aufmerksamkeit der CPU. Diese ist aber zeitlich begrenzt, den bei einem Multitasking-Betriebssystem laufen viele Prozess, auch in Form von Diensten bzw. Daemons, quasi gleichzeitigt. Wann welcher Prozess auf welche Ressource, wie CPU, zugreift regelt der **Scheduler** der Betriebssystems.  
![](Theorie_img/1.gif)

Anfangs erscheint es, dass es zwischen Prozessen und Threads keine Unterschiede gibt, den jeder Prozess besteht aus mindestens einem Thread (**Main Thread**). Doch jeder Prozess ist für die Ausführung seiner Threads verantwortlich, außerdem endet ein Prozess wenn alle seine Prozesse beendet sind. Somit besteht eine *has-a* Beziehung, ein Prozess besteht aus 1..* Threads, ein Thread kann aber ohne Prozess nicht existieren.  
![](Theorie_img/2.gif)

Prozesse sind schwergewichtiger als Threads, da bei Erstellung eines Prozesses der komplette Namensraum dupliziert werden muss. Diesen Aufwand spart man bei der Verwendung von Threads, da diese in einem gemeinsamen Adressraum ablaufen. Somit stehen den einzelnen Threads dasselbe Codesegment, Datensegment, der Heap und alle anderen Zustandsdaten, die ein "gewöhnlicher" Prozess besitzt, zur Verfügung – was somit auch die Arbeit beim Austausch von Daten und bei der Kommunikation untereinander erheblich erleichtert.   
![](Theorie_img/3.gif)

![](Theorie_img/4.gif)

# Nebenläufigkeit
Grundsätzlich eignen sich Prozesse und Threads zur "Parallelisierung" eines Programms, also das mehrere Anweisungen oder Befehle quasi gleichzeitig ausgeführt werden können. Echte Nebenläufigkeit kann man nur mit Prozessoren implementieren, wenn diese mehrere Kerne besitzen bzw. einen logischen Prozessoren aus mehreren (=Mehrprozessorsystem) bilden.

## Python
In Python sollte man die Klasse [multiprocessing](https://docs.python.org/3.7/library/multiprocessing.html#module-multiprocessing) anstatt der Klasse threading  verwenden. Multiprocessing setzt auf Prozesse, threading auf Threads um Nebenläufigkeit zu realisieren. Ein Problem ist, dass bei Python der Global Interpreter Lock (=GIL) nur einem Thread erlaubt, den Python Interpreter gleichzeitig in Besitz zu haben. Somit wird Code, der auf multi-threading, setzt von der Geschwindigkeit durch den GIL gebottleneckt.

### multiprocessing.Process
multiprocessing.Process(group=None, target=None, name=None, args=(), kwargs={}, \*, daemon=None)  
Erstellt einen neuen Prozess. Bei *target* kann die Zielmethode angegeben werden, die parallelisiert werden soll, bei *args* können dann ihre Parameter angegeben werden.

Wichtige Methoden:

* **run()** - Hier wird der Programmablauf des Prozesses definiert. Wenn man einen eigenen Prozess durch Vererbung erstellt, sollte diese Methode überschrieben werden, da sie bei **start()** aufgerufen wird.
* **start()** - Startet den Prozess nebenläufig.
* **join([timeout])** - Wartet bis der Prozess seine Arbeit getan hat und führt dann den Programmverlauf fort. Wenn ein **timeout** gesetzt wird, wartet `join()` nur maximal wie der gesetzte **timeout** Wert, somit läuft der Prozess im Hintergrund weiter. `join()` endet ihn nicht, bei beendeten Prozessen ruft er aber teilweise den **Garbage Collector** hervor.
* **terminate()** - Terminiert, endet den Prozess.
*  **close()** - Schließt das Prozess-Objekt und gibt alle mit ihm assoziierten Ressourcen frei. Dient zur schönen Schließung.

Grundsätzlich gilt, dass wenn man `close()` oder `terminate()` aufruft, dies vor `join()` machen sollte, damit Fehler im Bezug auf Speicher verhindert werden.

# Interprozesskommunikation
Bezeichnet den Informationsaustausch zwischen Prozessen eines Systems.

Es gibt mehrere Möglichkeiten und Mechanismen zur Umsetzung, hier sind einige aufgelistet:  

* Shared Memory
* Mutexe / Locks
* Pipes
* Queues
* Semaphore
* Events
* Bedingungsvariablen (Conditions)

## Shared Memory
Shared Memory (gemeinsamer Datenspeicher) ist der einfachste Weg Prozesse miteinander kommunizieren zu lassen. Dabei teilen sich Prozesse einen eigens geteilten Speicherbereich, auf denn alle Prozesse Zugriff haben.
![](Theorie_img/5.gif)
### Python
#### [multiprocessing.Value](https://docs.python.org/3.7/library/multiprocessing.html#multiprocessing.Value)
multiprocessing.Value(typecode_or_type, \*args, lock=True)  
Erstellt ein Objekt, dass einen Wert im Shared-Memory angelegt. Bei *typecode_or_type* muss der ctype angegeben werden, da im Shared-Memory grundsätzlich nur Daten von primitiven Datentypen angelegt werden. Hier kann entweder ein String z.B. **"i"** für *Integer* angeben oder man importiert `import ctypes` und verwendet `ctypes.c_int` für *Integer*. Weiters kann der Startwert angegeben werden z.B. `0` und mit `lock=` kann man sagen, ob ein Lock angelegt werden soll. Standardmäßig, wenn nichts angegeben wird, wird einer erstellt, wenn man dies nicht wünscht kann man `lock=False` als Parameter angeben.
Auf den Wert des Objektes kann mit `.value` zugegriffen und gearbeitet werden.
```python
from multiprocessing import Process, Value
import ctypes

class A(Process):
    """
    Prozess, der im einem Shared-Memory Objekt 100 speichert
    """
    def __init__(self, v):
        Process.__init__(self)
        self.v = v

    def run(self):
        # Speichern von 100
        self.v.value = 100


class B(Process):
    """
    Prozess, der den Wert eines Shared-Memory Objekts printed
    """
    def __init__(self, v):
        Process.__init__(self)
        self.v = v

    def run(self):
        print(str(self.v.value))

if __name__ == "__main__":
    # Shared-Memory Objekt definieren, dass einen Integer speichern kann
    v = Value(ctypes.c_int)
    # Prozess A diesen übergeben und starten
    a = A(v)
    a.start()
    # Prozess B diesen übergeben und starten
    b = B(v)
    b.start()
    # Sauber schließen
    a.join()
    b.join()
```
Da Operationen wie `+=` nicht atomar sind (bestehen aus mehreren Anweisungen), ist es nicht ausreichend für Datenintegrität nur einfach dem Wert so zu erhöhen:
```python
counter.value += 1
```
Man kann deshalb diesen Teil "locken" (mehr dazu im nächsten Abschnitt), damit nur ein Prozess gleichzeitig darauf zugreift.
```python
with counter.get_lock():
    counter.value += 1
```
#### [multiprocessing.Array](https://docs.python.org/3.7/library/multiprocessing.html#multiprocessing.Array)
multiprocessing.Array(typecode_or_type, size_or_initializer, \*, lock=True)
Erstellt ein Objekt, dass im Shared-Memory ein Array anlegegt. Wie bei *multiprocessing.Value* muss zuerst der c_type angegeben werden. Danach aber gibt man entweder die Array-Größe als Integer oder die gleich die Werte als Liste z.B: [100, 200]. Danach folgt der altbekannte Lock.  
Auf Werte kann wie bei Arrays über den Index mit Brackets zugegriffen werden.
```python
from multiprocessing import Process, Array
import ctypes


class A(Process):
    """
    Prozess, der im einem Shared-Memory Objekt 100 und 200 speichert
    """
    def __init__(self, arr):
        Process.__init__(self)
        self.arr = arr

    def run(self):
        # Speichern arron 100
        self.arr[0] = 100
        self.arr[1] = 200


class B(Process):
    """
    Prozess, der die Werte eines Shared-Memory Objekts printed
    """
    def __init__(self, arr):
        Process.__init__(self)
        self.arr = arr

    def run(self):
        print(str(self.arr[0]) + " " + str(self.arr[1]))

if __name__ == "__main__":
    # Shared-Memory Objekt definieren, dass aus einem
    # Integer-Array besteht, was zwei Werte speichern kann
    arr = Array(ctypes.c_int, 2)
    # Prozess A Objekt übergeben und starten
    a = A(arr)
    a.start()
    # Warten und sauber schließen, bis Werte gespeichert wurden
    a.join()
    # Prozess B Objekt übergeben und starten
    b = B(arr)
    b.start()
    # Sauber schließen
    b.join()
```
Wie bei `Value` kann man auch `Array`-Objekte locken, um nicht atomare Anweisungen vor ungeplanten Prozesszugriff zu schützen. Dabei muss nicht mal die `get_lock()`-Methode wie bei `Value` aufgerufen werden, man muss nur `with array_variable` angeben.

## Mutexe / Locks
Geteilter Speicher zwischen Prozessen ist zwar relativ einfach zu realisieren, allerdings konkurrieren oft mehrere Prozesse um die selbe zu Ressourcen bzw. stören sich gegenseitig bei kritischen Operationen. Daher muss alles synchronisiert werden und kritische Bereiche mit Locks gesichert werden.  
Der Unterschied zwischen Lock und Mutex ist, dass Mutex auch systemweit gelten können. Sonst sind ähnlich bis gleich (hängt von der Implementierung ab).
### Python
#### [multiprocessing.Lock](https://docs.python.org/3.7/library/multiprocessing.html#multiprocessing.Lock)
Erzeugt ein Lock-Objekt, das den Programmverlauf stoppt eines Programmverlaufs auf einem Punkt stoppt, wenn ein anderer Prozess den Lock gerade für sich beansprucht.

Wichtige Methoden:

* **acquire()** - Zwei Funktionen, entweder beansprucht ein Prozess den Lock und führt seinen Programmverlauf fort, oder ein Prozess wartet an dieser Programmstelle, bis der Lock freigegeben wird.
* **release()** - Gibt den Lock frei, so das ein anderer Prozess ihn beanspruchen kann.

In Bezug auf Locks ist es aber gängiger, diese mit dem `with`-Statement zu benutzen.
```python
with lock_variable:
  # Code
```
Bei der Definition von `with` wird automatisch `acquire()` ausgeführt, nachdem der Block fertig ist 'release()'.
```python
from multiprocessing import Process, Lock
import os


def sayhello(lock, num):
    """
    Schreibt Nachrichten nacheinander in ein File
    und verwendet dabei eine Lock, da mehrere
    Prozesse gleichzeitig darauf zugreifen.
    :param lock: die zu verwendende Lock
    :param num: die Prozessnummer
    :return: None
    """
    with open("foo.txt", "ab") as fo:
        for i in range(100):
            # Ohne Lock findet keine geregelte Abfolge statt und Prozesse behindern sich gegenseitig beim schreiben
            # so dass merkwürdige Ausgaben entstehen, weil die Abfolge vom Schreiben (in den Buffer) und dem Speichern
            # (im file durch flush [Buffer -> file]) gestört wird
            with lock:
                fo.write("Process ".encode())
                fo.flush()
                fo.write(str(num).encode())
                fo.flush()
                fo.write(": Hello!\n".encode())
                fo.flush()


if __name__ == "__main__":
    # File loeschen, falls es bereits existiert
    if os.path.isfile("foo.txt"):
        os.remove("foo.txt")
    # Lock erstellen
    lock = Lock()
    # Zehn Prozesse erstellen und starten
    for num in range(10):
        print("Starting Process %i" % (num))
        Process(target=sayhello, args=(lock, num)).start()

```

## Pipes
Dienen zur Kommunikation zwischen zwei Prozessen. Pipes arbeiteten nach dem FIFO-Prinzip. Grundsätzlich sollten Zugriffsrechte vergeben werden, also wer sendet oder wer empfängt.
### Python
#### [multiprocessing.Pipe](https://docs.python.org/3.7/library/multiprocessing.html#multiprocessing.Pipe)  
Aufruf `Pipe()` gibt ein Paar von [Connections](https://docs.python.org/3.7/library/multiprocessing.html#multiprocessing.connection.Connection) (conn1, conn2) zurück. Diese kann man beispielsweise Prozessen übergeben, die mit diesem die Connection-Methoden verwenden können, um damit Daten zu senden `send()` oder empfangen `recv()`. Man muss aber aufpassen, gleichzeitiges Senden oder Empfangen führt zu Fehlern!
```python
from multiprocessing import Process, Pipe


class Message:
    """
    Simples Objekt, welches eine Nachricht repräsentiert
    """
    def __init__(self, priority, message):
        """
        Erstellt neues Message-Objekt
        :param priority: Priorität der Nachricht
        :param message: Inhalt der Nachricht
        """
        self.priority = priority
        self.message = message

    def __str__(self):
        """
        Gibt die Nachricht schön als String formatiert zurück
        :return: Nachricht als String
        """
        return "Priority: " + str(self.priority) + " Message: " + str(self.message)


class Sender(Process):
    """
    Prozess, welcher einen Teil einer Pipe-Connection benutzt um zu senden
    """
    def __init__(self, con):
        """
        Erstellt ein Sender-Objekt
        :param con: Connection für die Pipe ("Nachrichtentunnenl")
        """
        Process.__init__(self)  # Vererbung, __init__ von Superklasse aufrufen
        self.con = con

    def run(self):
        """
        Wenn der Prozess startet, wird diese Methode ausgeführt.
        Sendet Nachrichten an den Empfänger
        :return:
        """
        # Sendet String
        self.con.send("Hallö")
        # Sendet ganzes Objekt
        self.con.send(Message("High", "Secret Message!"))
        # Verbindung wird geschlossen
        self.con.close()


class Receiver(Process):
    def __init__(self, con):
        """
        Erstellt ein Empfänger-Objekt
        :param con: Connection für die Pipe ("Nachrichtentunnenl")
        """
        Process.__init__(self)  # Vererbung, __init__ von Superklasse aufrufen
        self.con = con

    def run(self):
        """
        Wenn der Prozess startet, wird diese Methode ausgeführt.
        Empfängt Nachrichten vom Sender
        :return:
        """
        # Empfängt String und gibt ihn aus
        var = self.con.recv()
        print("Received string: " + var)
        # Empfängt Objekt und gibt ihn aus
        var = self.con.recv()
        print("Received object: " + str(var))
        # Verbindung wird geschlossen
        self.con.close()


if __name__ == "__main__":
    # Connections durch Erstellen einer Pipe bekommen
    inPut, outPut = Pipe()
    # Diese den Objekten von Sender und Empänger zuweisen und diese auch initalisieren
    s = Sender(inPut)
    r = Receiver(outPut)
    # Starten der Prozesse
    s.start()
    r.start()
    # Sauberes Schließen
    s.join()
    r.join()
```

## Queues
Queues sind Warteschlangen die nach dem FIFO-Prinzip arbeiten. In diese werden Elemente reingegeben, die nach der Reihe (erster drinnen, erster draußen) rausgenommen werden. Im Gegensatz zu Pipes kann es mehr Teilnehmer geben, als nur die zwei einer Pipe.
### Python
#### [multiprocessing.Queue](https://docs.python.org/3.7/library/multiprocessing.html#multiprocessing.Queue)
multiprocessing.Queue([maxsize])  
Erstellt eine Queue, die, wenn keine maximale Größe als Parameter übergeben wurde, theoretisch unendlich groß ist. Queues verwenden im Hintergrund Pipes zur Realisierung, jedoch können mehrere Prozesse auf eine Queue zugreifen.

Wichtige Methoden:

* **empty()** - Schaut ob die Queue leer ist und gibt `True` aus, wenn diese leer ist, sonst `False`.
* **put(obj)** - Fügt ein Objekt der Queue hinzu.
* **get()** - Gibt das "oberste" Objekt (zuerst hinzugefügte) aus der Queue zurück und entfernt es aus dieser.

```python
from multiprocessing import Process, Queue


class SenderProcess(Process):
    """
    Stellt einen einfachen Sender dar, welcher
    eine Nachricht in eine Queue stellt.
    """
    def __init__(self, queue):
        """
        Initialisiert die Basisklasse Process
        :param queue: die Queue, in welche geschickt wird
        """
        Process.__init__(self)
        self.queue = queue

    def run(self):
        """
        Schickt eine Nachricht in die Queue
        :return: None
        """
        self.queue.put("Hallo Queue!")


class SecondSenderProcess(Process):
    """
    Stellt einen einfachen Sender dar, welcher
    eine Nachricht in eine Queue stellt.
    """
    def __init__(self, queue):
        """
        Initialisiert die Basisklasse Process
        :param queue: die Queue, in welche geschickt wird
        """
        Process.__init__(self)
        self.queue = queue

    def run(self):
        """
        Schickt eine Nachricht in die Queue
        :return: None
        """
        self.queue.put("Grüß Gott Queue!")


class ReceiverProcess(Process):
    """
    Ein einfacher Empfänger einer Queue.
    """
    def __init__(self, queue):
        """
        Initialisiert die Basisklasse Process
        :param queue: die Queue, in welche geschickt wird
        """
        Process.__init__(self)
        self.queue = queue

    def run(self):
        """
        Empfängt die Nachricht und gibt sie aus.
        :return: None
        """
        # Schauen ob Queue nicht leer ist
        # Sonst tritt eine Exception auf
        while not self.queue.empty():
            print(self.queue.get())


if __name__ == "__main__":
    # Erstellt eine Queue ohne Größenbeschränkung
    q = Queue()
    # Sender Prozesse starten
    sender = SenderProcess(q)
    sender.start()
    sender2 = SecondSenderProcess(q)
    sender2.start()
    # Warten und sauber schließen, damit Werte immer
    # immer gesetzt werden
    sender.join()
    sender2.join()
    # Empänger Prozess starten
    receiver = ReceiverProcess(q)
    receiver.start()
    # Sauber schließen
    receiver.join()

```
## Semaphore
Eine Semaphor ähnelt einem Lock, doch anstatt nur einen Prozess zu erlauben, werden beim Semaphor eine bestimme Anzahl von Prozessen in einem kritischen Abschnitt erlaubt. Ein Semaphor stellt sozusagen die "Anzahl an freien Plätzen" in
einem kritischen Abschnitt dar.
### Python
#### [multiprocessing.Semaphore](https://docs.python.org/3.7/library/multiprocessing.html#multiprocessing.Semaphore)
multiprocessing.Semaphore([value])  
Erstellt einen Semaphor, der eine übergebene Anzahl von Prozessen erlaubt. Standardmäßig wird 1 gesetzt. Semaphore besitzen die gleichen Methoden wie Locks.

Wichtige Methoden:

* **acquire()** - Zwei Funktionen, einer definierten Anzahl von Prozessen wird der weitere Programmverlauf erlaubt, andere Prozesse warten hingegen bis wieder die definierte Anzahl unterschritten ist und wieder ein Prozess vom Semaphor Zugriff bekommt.
* **release()** - Ein Prozess gibt seinen Semaphor frei, der interne Counter wird um eins verringert und somit kann ein anderer Prozess in den kritischen Abschnitt eintreten.

Semaphore können auch mit dem `with`-Statement benutzt werden (wie Locks).
```python
with semaphore_variable:
  # Code
```

```python
from multiprocessing import Process, Semaphore
import os

def writefile(semaphore, num):
    """
    Schreibt Nachrichten nacheinander in getrennte Files
    und verwendet dabei einen Semaphor, damit nicht zu viele
    Prozesse gleichzeitig das Filesystem belasten.
    :param semaphore: der zu verwendende Semaphor
    :param num: die Prozessnummer
    :return: None
    """
    # Mitteilen, dass man auf Semaphor-Freigabe wartet
    print("Process "+str(num)+": Waiting for semaphore...")
    # Semaphor, der zwei Prozesse zulässt
    with semaphore:
        # Mitteilen, dass man einer der beiden Prozesse ist
        # die gerade auf kritischen Teil zugreifen
        print("Process "+str(num)+": Acquired semaphore!")
        # Bestimmtes File öffnen
        with open("foo"+str(num)+".txt", "ab") as fo:
            # Viele Zeilen schreiben
            for i in range(500000):
                fo.write("Process ".encode())
                fo.write(str(num).encode())
                fo.write(": Hello!\n".encode())
    # Mitteilen, dass man fertig ist
    print("Process "+str(num)+": Released semaphore!")

if __name__ == "__main__":
    # Files löschen, falls sie bereits existieren
    for num in range(10):
        if os.path.isfile("foo"+str(num)+".txt"):
            os.remove("foo"+str(num)+".txt")
    # Zwei Prozesse dürfen gleichzeitig schreiben
    sem = Semaphore(value=2)
    # Zehn Prozesse erstellen und starten
    for num in range(10):
        Process(target=writefile, args=(sem, num)).start()
```
## Sockets
![](Theorie_img/8.PNG)

Ein Sockets

* ist ein Kommunikationsendpunkt
* wird über IP-Adresse und Port eindeutig identiﬁziert
* nimmt eingehende Verbindungen entgegen
* verbindet sich mit seinem Gegenstück

Unter einer IP-Adresse können mehrere Sockets erreichbar sein! (Anderen Port benutzen)

Ein Socket realisiert somit eine Kommunikationsschnittstelle zwischen Prozessen, die meist auf **verschiedenen** Systemen laufen.

Sowohl Server als auch Client benötigen einen eindeutigen Kommunikationsendpunkt  
⇒ Wir unterscheiden zwischen Server-Socket und Client-Socket

#### Server-Socket
* Wartet und "horcht" auf eingehende Verbindungen durch Clients (engl. " to listen")
* Wird daher auch passiver Socket genannt
* Muss einer IP-Adresse und einem Port zugeteilt sein ( "Binding")
* Empfängt Daten von Clients, führt Berechnungen durch und sendet das Ergebnis zurück
* Typischerweise Multi-Threaded – mehrere Clients, mehrere Threads!

#### Client-Socket
* Verbindet sich mit Server-Socket und initiiert den Verbindungsaufbau
* Wird daher auch aktiver Socket genannt
* Muss IP-Adresse und Port des Servers wissen
* Sendet Daten an Server und empfängt die Resultate
* Multi-Threaded oder Single-Threaded

#### Protokoll
* Sockets setzen auf der vierten Schicht auf und beﬁnden sich selbst auf Layer 5
* Je nach verwendetem Protokoll auf Schicht 4 unterscheiden wir zwischen
  * TCP-Sockets ("Stream")
  * UDP-Sockets ("Datagram")
* Das zugrunde liegende Protokoll wirkt sich maßgeblich auf die Programmierung von Sockets aus

##### TCP
![](Theorie_img/9.PNG)

##### UDP
![](Theorie_img/10.PNG)

##### Anwendungsgebiete
TCP-Sockets | UDP-Sockets
---|---
Geschäftsanwendungen | Audio-Streaming
WWW| Video-Streaming
Webservices |Computerspiele
 Login |Alive-Messages
 Datenbankanwendungen |Live-Daten (Aktien, Wetter, ...)

 Grundsätzlich wird meist TCP verwendet, außer die Übertragung muss nicht sichergestellt werden.

### Java
#### [Socket](https://docs.oracle.com/javase/7/docs/api/java/net/Socket.html) & [ServerSocket](https://docs.oracle.com/javase/7/docs/api/java/net/ServerSocket.html)
Die Klasse Socket wird für Client-Sockets verwendet, die Klasse ServerSocket für den Server-Sockets.

Mit `Socket(InetAddress host, int port)` kann ein Client-Socket, der sich zum ServerSocket verbindet, der zu den Parametern passt (host = IP-Adresse des Servers, Port = Port des Servers) erstellt werden. Beim Konstruktor vom ServerSocket muss nur der Port angegeben werden `ServerSocket(int port)`.

Der Server-Socket kann eine Verbindung, die der Client aufbauen möchte, mit der Methode `accept()` akzeptieren, die Methode gibt dabei einen Socket zurück, der zur Kommunikation zum Client dient. Diesen kann man dann beispielsweise Threads zur Abarbeitung übergeben.

Die Kommunikation erfolgt mithilfe von Streams. **OutputStreams** fungieren als Pendant zur Pythons `send()` Methode, **InputStreams** zu `recv()`. Dazu können beispielsweise die Klassen [PrintWriter](https://docs.oracle.com/javase/7/docs/api/java/io/PrintWriter.html), [ObjectOutputStream](ObjectOutputStream) als OutputStream und [BufferedReader](https://docs.oracle.com/javase/7/docs/api/java/io/BufferedReader.html), [ObjectInputStream](https://docs.oracle.com/javase/7/docs/api/java/io/ObjectInputStream.html) als InputStream verwendet verwenden. Bei ihrer Erstellung müssen die Parameter des Sockets übergeben werden, d.h. bei OutputStreams muss die Methode `getOutputStream()` am Socket-Objekt aufgerufen werden, bei InputStreams `getInputStream()`.

Zum Senden und Empfangen müssen die jeweiligen Methoden der Stream-Klassen benutzt werden. Bei ObjectOutputStreams kann man mit `writeObject(Object obj)` Objekte verschicken und mit einem InputStream mit `readObject()` empfangen.

```java
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Einfaches Beispiel mit Socket und Client,
 * wo Client(s) Nachrichten vom Server empfangen
 * @author Kacper Urbaniec
 * @version 16.12.2018
 */

/**
 * Server, sendet Nachrichten an Clients
 */
public class Server {
    private ExecutorService ex;
    private ArrayList<ServerThread> threads;
    private ServerSocket s;

    /**
     * Erzeugt neues Server-Objekt
     * @throws IOException
     */
    public Server() throws IOException {
        ex = Executors.newCachedThreadPool();
        threads = new ArrayList<>();
        s = new ServerSocket(5555);
    }

    /**
     * Verarbeitet Socket-Verbindungsanfragen der Clients und
     * erstellt Threads um Funktionalität zu realisieren
     */
    public void work() {
        try {
            while (true) {
                // Neuen Thread fürs Arbeiten erstellen, wenn ein Client
                // sich verbinden will (s.accept())
                ServerThread worker = new ServerThread(s.accept());
                // Diesen einer Liste hinzufügen und per ExecutorService
                // starten
                threads.add(worker);
                ex.execute(worker);
            }
        }
        catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    // Startet den Server
    public static void main(String[] args) {
        try {
            Server server = new Server();
            server.work();

        }
        catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}

/**
 * Implementiert Funktionlität des Servers zu Clients
 */
class ServerThread extends Thread{
    private Socket s;
    private PrintWriter out;

    /**
     * Erzeugt neuen Thread
     * @param s Socket, der Verbindung zum Client defniert
     * @throws IOException
     */
    public ServerThread(Socket s) throws IOException{
        super();
        this.s = s;
        // Zweiter Parameter true bewirkt, dass flush() automatisch nach
        // einem write() oder print() gemacht wird
        this.out = new PrintWriter(s.getOutputStream(), true);
    }

    /**
     * Sendet alle zwei Sekunden einen String an den Client
     */
    @Override
    public void run() {
        try {
            while (true) {
                // String definieren und schreiben
                this.out.println("Hallo Client!");
                // Zwei Sekunden warten
                Thread.sleep(2000);
            }
        }
        catch(Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}

/**
 * Client, empfängt Nachrichten vom Server
 */
class Client {
    private Socket s;
    private BufferedReader in;

    /**
     * Erzeugt neues Client-Objekt
     * @throws IOException
     */
    public Client() throws IOException {
        s = new Socket("localhost", 5555);
        this.in = new BufferedReader(new InputStreamReader(s.getInputStream()));
    }

    /**
     * Empfängt Nachrichten vom Server
     */
    public void work() {
        try {
            while (true) {
                // Empfängt Nachricht und gibt sie aus
                String message = this.in.readLine();
                System.out.println(message);
            }
        }
        catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }

    // Startet Client
    public static void main(String[] args) {
        try {
            Client client = new Client();
            client.work();

        }
        catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}
```

```java
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
* Einfaches Beispiel mit Socket und Client,
* wo Client(s) Objekte vom Server empfangen
* @author Kacper Urbaniec
* @version 16.12.2018
*/

/**
 * Server, sendet Objekte an Clients
 */
public class ServerObj {
    private ExecutorService ex;
    private ArrayList<ServerObjThread> threads;
    private ServerSocket s;

    /**
     * Erzeugt neues Server-Objekt
     * @throws IOException
     */
    public ServerObj() throws IOException {
        ex = Executors.newCachedThreadPool();
        threads = new ArrayList<>();
        s = new ServerSocket(5555);
    }

    /**
     * Verarbeitet Socket-Verbindungsanfragen der Clients und
     * erstellt Threads um Funktionalität zu realisieren
     */
    public void work() {
        try {
            while (true) {
                ServerObjThread worker = new ServerObjThread(s.accept());
                threads.add(worker);
                ex.execute(worker);
            }
        }
        catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    // Startet den Server
    public static void main(String[] args) {
        try {
            ServerObj server = new ServerObj();
            server.work();

        }
        catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}

/**
 * Implementiert Funktionlität des Servers zu Clients
 */
class ServerObjThread extends Thread{
    private Socket s;
    private ObjectOutputStream out;
    private Random random;

    public ServerObjThread(Socket s) throws IOException{
        super();
        this.s = s;
        // Verwendet anstatt PrintWriter ObjectStream, um Objekte
        // zu verschicken
        this.out = new ObjectOutputStream(s.getOutputStream());
        random = new Random();
    }

    /**
     * Sendet alle zwei Sekunden einen String und einen zufälligen
     * Integer an den Client
     */
    @Override
    public void run() {
        try {
            while (true) {
                this.out.writeObject("Sende Integer: ");
                Object i = new Integer(random.nextInt(100));
                this.out.writeObject(i);
                Thread.sleep(2000);
            }
        }
        catch(Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}

/**
 * Client, empfängt objekte vom Server
 */
class ClientObj {
    private Socket s;
    private ObjectInputStream in;

    /**
     * Erzeugt neues Client-Objekt
     * @throws IOException
     */
    public ClientObj() throws IOException {
        s = new Socket("localhost", 5555);
        this.in = new ObjectInputStream(s.getInputStream());

    }

    /**
     * Empfängt Objekte vom Server
     */
    public void work() {
        try {

            while (true) {
                String message = (String)this.in.readObject();
                System.out.print(message);
                Object obj = this.in.readObject();
                System.out.println(obj.toString());
            }
        }
        catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }

    // Startet Client
    public static void main(String[] args) {
        try {
            ClientObj client = new ClientObj();
            client.work();

        }
        catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}


```

## Events

### Python
#### [multiprocessing.Event](https://docs.python.org/3.7/library/multiprocessing.html#multiprocessing.Event)
multiprocessing.Event()  
Erzeugt ein Event-Objekt, was eine interne Flag besitzt. Mit der Methode `set()` wird es auf **True** festgelegt, alle Methoden die ein `event_variable.wait()` implementieren führen ihre Arbeit fort, bis jemand die Flag mit `clear()` wieder auf **False** stellt.

Wichtige Methoden:

* **set()** - Interner Flag wird auf True gesetzt.
* **clear()** - Interner Flag wird auf False gesetzt.
* **wait()** - Blockiert weiteren Programmverlauf (in einem Prozess), wenn das Flag durch `clear()` auf False gesetzt wurde. Programmverlauf wird durch Aufruf von `set()` fortgesetzt.

```python
from multiprocessing import Process, Value, Event

class CalculatorProcess(Process):
    """
    Stellt einen simplen Worker-Prozess dar,
    welcher auf ein Event wartet und dann
    die Summe von 1 bis n berechnet. Das Ergebnis
    wird in einen geteilten Speicher gelegt.
    """
    def __init__(self, v, event):
        """
        Initialisiert die Basisklasse Process
        :param v: ein geteilter Wert
        :param event: das Event, auf welches gewartet wird
        """
        Process.__init__(self)
        self.v = v
        self.event = event

    def run(self):
        """
        Wartet auf das Event und legt das Ergebnis
        in den geteilten Speicher
        :return: None
        """
        self.event.wait()
        summe = 0
        for i in range(self.v.value+1):
            summe += i
        self.v.value = summe


if __name__ == "__main__":
    v = Value('i', 0)
    e = Event()
    # Prozess starten
    calculator = CalculatorProcess(v, e)
    calculator.start()
    n = int(input("Bis zu welcher Zahl möchten Sie die Summe von 1 bis n berechnen?"))
    v.value = n
    # Signal geben, sobald Benutzer Eingabe getätigt hat
    e.set()
    calculator.join()
    print("Das ergebnis ist " + str(v.value))

```
## Bedingungsvariablen (Conditions)

### Python
#### [multiprocessing.Condition](https://docs.python.org/3.7/library/multiprocessing.html#multiprocessing.Condition)
multiprocessing.Condition([lock])  
Eine Condition mixt einen Lock und ein Benachrichtigungssystem. Grundsätzlich existiert ein Lock, doch der Prozess, der gerade im Lock ist, sagt auch welcher Prozess weitermachen soll, es können aber auch alle Prozesse angestupst werden.

Wichtige Methoden:

* **acquire()** - Zwei Funktionen, entweder beansprucht ein Prozess den Lock und führt seinen Programmverlauf fort, oder ein Prozess wartet an dieser Programmstelle, bis der Lock freigegeben wird.
* **release()** - Gibt den Lock frei, so das ein anderer Prozess ihn beanspruchen kann.
* **wait(timeout=None)** - Gibt den Lock frei und blockiert ihn später wieder, bis er wieder durch ein `notify()` angesprochen wird.
* **notify(n=1)** - Standardmäßig wird der nächste Process "aufgewacht", der auf die Condition durch `wait` wartet. Es können mehr Prozesse zur Weiterarbeit gebracht werden durch beispielsweise den Parameter 2 setzen zwei Prozesse ihre Arbeit fort.

Mit `with condition_name` kann wieder der Lock gesetzt werden.
```python
from multiprocessing import Process, Condition
import time
import os


def sayhello(condition, num):
    """
    Schreibt Nachrichten nacheinander in ein File
    und verwendet dabei eine Condition, da mehrere
    Prozesse gleichzeitig darauf zugreifen.
    :param condition: die zu verwendende Condition
    :param num: die Prozessnummer
    :return: None
    """
    with condition:
        # Auf Signal warten
        condition.wait()
        with open("foo.txt", "ab") as fo:
            for i in range(100):
                fo.write("Process ".encode())
                fo.flush()
                fo.write(str(num).encode())
                fo.flush()
                fo.write(": Hello!\n".encode())
                fo.flush()
        condition.notify()


if __name__ == "__main__":
    # File loeschen, falls es bereits existiert
    if os.path.isfile("foo.txt"):
        os.remove("foo.txt")
    condition = Condition()
    # Zehn Prozesse erstellen und starten
    for num in range(10):
        Process(target=sayhello, args=(condition, num)).start()
    time.sleep(1)
    # Den Vorgang "anstossen", da ja alle Prozesse
    # zu Beginn warten
    with condition:
        condition.notify()
```

# Probleme
## Race Condition
Eine Race condition oder race hazard bezeichnet in der Programmierung eine Konstellation, in der das Ergebnis einer Operation vom zeitlichen Verhalten bestimmter Einzeloperationen abhängt. Der Begriff stammt von der Vorstellung, dass zwei Signale wettlaufen, um die Ausgabe als erstes zu beeinflussen. Es fuhrt zu Fehlern, wenn das "Wettlaufen" nicht im Sinne des Programmierers abläuft.

### Beispiel
Zwei Threads (oder Prozesse) erhöhen den Wert einer Variable, Startwert 0, um eins. Somit sollte ja zwei rauskommen, oder?

Bei synchroner Abfolge, also idealer Abfolge kommt das ersuchte Ergebnis:  

Thread1 | Thread2 | | Integer Wert
--- | --- | --- | ---
| | | 0
read value| |		←|	0
increase value|||			0
write back||		→	|1
|read value|	←|	1
|increase value	||	1
|write back|	→|	2

Bei Parallelisierung treten aber schnell unbedachte Logikfehler auf:

Thread1 | Thread2 | | Integer Wert
--- | --- | --- | ---
| | |0
read value||		←	|0
|read value	|←|	0
increase value|||			0
|increase value	||	0
write back||		→|	1
|write back	|→|	1

### Python
Dieses Programm inkrementiert und dekrementiert parallel einen Wert, eigentlich sollte 0 rauskommen, aber der asynchrone Zugriff führt zu einer zufälligen Zahl.
```python
from multiprocessing import Process, Value

COUNT = 10000000
def foo(x):
    for i in range(COUNT):
        x.value += 1

def bar(x):
    for i in range(COUNT):
        x.value -= 1


if __name__ == "__main__":
    v = Value("i", 0, lock=False)
    t1 = Process(target=foo, args=(v,))
    t2 = Process(target=bar, args=(v,))
    t1.start()
    t2.start()
    t1.join()
    t2.join()
    print(v.value)
```
## Dead-Lock
Deadlock bezeichnet in der Informatik einen Zustand, bei dem eine zyklische Wartesituation zwischen mehreren Prozessen auftritt, wobei jeder beteiligte Prozess auf die Freigabe von Betriebsmitteln wartet, die ein anderer beteiligter Prozess bereits exklusiv belegt hat.

Der Zustand eines Deadlocks kann folgendermaßen definiert werden: Eine Menge von Prozessen befindet sich in einem Deadlock, wenn jeder dieser Prozesse auf ein Ereignis wartet, das nur ein anderer Prozess aus dieser Menge verursachen kann.

Deadlocks treten auf, wenn alle vier Bedingungen zutreﬀen:

* Mutual Exclusion: Exklusiver Zugriﬀ auf Ressourcen ist möglich
* Hold and Wait: Ressourcen können blockiert werden, während der Prozess (Thread) selbst auf eine andere Ressource wartet
* No Preemption: Zugewiesene Ressourcen können einem Prozess nicht mehr weggenommen werden
* Circular Wait: Es kommt zu einer zirkulären geschlossenen Kette von Prozessen, die aufeinander warten

#### Beispiel
Thread1 | Thread2
---|---
Lock 1 sperren |
↓ |
Counter 1 erhöhen |
↓ | Lock 2 sperren
↓ |↓
 ↓ | Counter 2 erhöhen
 Warten bis Lock 2 frei wird | ↓
  ↓ | Warten bis Lock 1 frei wird
  ↓ |↓
   ...| ...

#### Entgegenwirken
Es gibt verschiedene Strategien gegen Deadlocks:

* Deadlock Prevention (Verhütung): Man lässt Deadlocks durch Verhinderung einer der vier Bedingungen gar nicht erst entstehen, z.B. durch das Design unseres Programms
* Deadlock Avoidance (Vermeidung): Man versucht rechtzeitig zu erkennen, dass ein Deadlock entstehen könnte, und versucht darauf zu reagieren  
* Deadlock Detection (Erkennung): Zyklische Beziehungen werden erkannt und aufgelöst (z.B. Terminierung eines Prozesses / Threads)
* Ostrich Algorithmus (Vogelstrauß): Deadlocks ignorieren und passieren lassen


## Live-Lock
Bei einer unsauberen Implementierung kann man mit einer Deadlock-Avoidance-Strategie schnell in einen Livelock laufen. Ein Livelock ist dem Deadlock sehr ähnlich, im Unterschied zum Deadlock verharren mehrere Prozesse / Threads jedoch nicht im selben Zustand (z.B. Schlafend), sondern wechseln permanent ihren Zustand

#### Beispiel
* Zwei Threads wollen dieselben zwei Ressourcen beanspruchen
* Thread 1 beansprucht Lock 2, Thread 2 beansprucht Lock 1
* Sie wollen die jeweils andere Ressource beanspruchen
* Sie erkennen die Deadlock-Gefahr und geben beide ihre Ressourcen frei
* Beide warten dieselbe Zeit und beginnen von Neuem

## Starvation
Beschreibt das Problem, dass ein Prozess eine benötigte Ressource für eine lange Zeit nicht (vom Scheduler) zugeteilt bekommt.

Oft basiert diese "Verhungerung" auf Logikfehlern vom Scheduler-Algorithmen.

* Shortest Job First Algorithmus - Ein größerer Prozess kann nie abgearbeitet werden, da "leichtgewichtigere", kleinere Prozesse immer vor ihm gestellt werden.
* Priority Scheduling - Wenn dauernd Prozesse mit hoher Priorität einkommen, müssen Prozesse mit niedrigerer Priorität solange werten, bis diese abgearbeitet sind.

> Rumor has it that, when they shut down the IBM 7094 at MIT in 1973, they found a low-priority process that had been submitted in 1967 and had not yet been run.

# Quellen
http://openbook.rheinwerk-verlag.de/c_von_a_bis_z/026_c_paralleles_rechnen_003.htm

https://docs.python.org/3.7/library/multiprocessing.html#module-multiprocessing

https://realpython.com/python-gil/

https://stackoverflow.com/questions/2332765/lock-mutex-semaphore-whats-the-difference

https://de.wikipedia.org/wiki/Interprozesskommunikation#Benannte_Pipes_(FIFO-Pipes)

https://en.wikipedia.org/wiki/Race_condition

https://de.wikipedia.org/wiki/Deadlock_(Informatik)

https://www.quora.com/What-does-%E2%80%98starvation%E2%80%99-mean-in-operating-systems

Softwareentwicklung 4, Dominik Dolezal, 20. November 2016
