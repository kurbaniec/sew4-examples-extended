# Everything C# #  
C# (gesprochen als c sharp) ist eine typsichere, objektorientierte Allzweck-Programmiersprache. Architekt der Sprache ist Anders Hejlsberg im Auftrag von Microsoft. Die Sprache ist an sich plattformunabhängig, wurde aber im Rahmen der .NET-Strategie entwickelt, ist auf diese optimiert und meist in deren Kontext zu finden. Microsoft bezeichnet seine Implementierung als Visual C#.

Historisch wurde in C# fast exklusiv für Windows entwickelt. Durch Xamarin ist es inzwischen aber auch möglich, für macOS, iOS und Android zu entwickeln. Zudem gibt es mit .NET Core auch offizielle Unterstützung für GNU/Linux und macOS.

## Einstieg: Einfaches Programm
Wir fangen mit einem einfachen Programm an, dass berechnen soll wieviel Holz und Glass für die Herstellung eines Fensters vonnöten ist. Keine Sorge, wir gehen hier nicht tiefgreifend auf die Thematik der Fenster-Herstellung ein, sondern schauen uns ein paar Grundelemente der Programmiersprache an.
```cs
using System;

namespace FirstProgram
{
    class GlazerCalc
    {
        static void Main()
        {
            double width, height, woodLength, glassArea;
            string widthString, heightString;

            widthString = Console.ReadLine();
            width = double.Parse(widthString);

            heightString = Console.ReadLine();
            height = double.Parse(heightString);

            woodLength = 2 * ( width + height ) * 3.25 ;

            glassArea = 2 * ( width * height ) ;

            Console.WriteLine ( "The length of the wood is " +
            woodLength + " feet" ) ;
            Console.WriteLine( "The area of the glass is " +
            glassArea + " square metres" ) ;
        }
    }
}
```
### Wie führe ich das Program aus?
Falls man Visual Studio verwendet:  
Das Projekt GlazerCalc öffnen (unter CSharp_Examples zu finden) und mit CTRL+F5 ausführen (mit CTRL schließt sich das aufpoppende Konsolenfenster nicht sofort, damit man die Ausgabe sieht).

Alternativ kann man nur per C#-Compiler (csc.exe) in der Command-Line arbeiten. Dazu muss dieser zum Path hinzugefügt werden, damit dieser dort aufrufbar ist. Persönlich musste ich folgenden Pfad in den Systemumgebungsvariablen hinzufügen: `C:\Windows\Microsoft.NET\Framework64\v4.0.30319`. Jetzt navigiert man zum Sourcefile wie **GlazerCalc.cs** und kompiliert dieses:
```bash
csc GlazerCalc.cs
```
Es entsteht eine ausführbare Datei **GlazerCalc.exe**, die man einfach mit `GlazerCalc` ausführen kann.

### Namespace: using System & namespace FirstProgram
Mit der Anweisung `using System;` sagen wir dem C#-Compiler, dass wir was aus dem *namespace* System benutzen wollen. Ein Namespace definiert einen Bereich von verwandten Objekten. Er dient hauptsächlich zur Organisierung von Code-Elementen.   Theoretisch könnte man die Anweisung `using System;` weglassen, doch möchte man dann auf Klassen des Namespaces System zugreifen, müsste man dies voll ausschreiben wie `System.Console.WriteLine("C")`. Weiteres Beispiel:
```cs
namespace SomeNameSpace
{
    public class MyClass
    {
        static void Main()
        {
            Nested.NestedNameSpaceClass.SayHello();
        }
    }

    // a nested namespace
    namespace Nested   
    {
        public class NestedNameSpaceClass
        {
            public static void SayHello()
            {
                Console.WriteLine("Hello");
            }
        }
    }
}
// Output: Hello
```
### class GlazerCalc
Ein C# Programm besteht aus einer oder mehreren Klassen. Eine Klasse bildet einen Container der Daten und Programm-Code enthält, um eine Aufgabe zu lösen. Grundsätzlich sollte der Klassenname auch der Programmname sein, damit hat Programm mit der Klasse GlazerCalc den Dateinamen **GlazerCalc.cs** (.cs ist die Dateiendung von C#).

### Console
Methoden im Zusammenhang mit der Command-Line:  
`Console.ReadLine();` -> Einlesen von Benutzereingaben  
`Console.WriteLine ("Some String")` -> Ausgabe eines als Parameter übergebenen Strings

### Console.WriteLine ( "The length of the wood is " + woodLength + " feet" ) ;
Wie in Java wird beim hinzufügen eines Objektes zu einem String seine String-Repräsentation angehängt.    
Aufpassen! `Console.WriteLine ( 2.0 + 3.0 );` würde 5 ausgeben, während `Console.WriteLine ( "2.0" + 3.0 );` 2.03 ausgibt!

### Keyword static
Wie in Java sagt man damit, dass etwas zu einer Klasse gehört (und dort einmal vorkommt) und nicht zu einem spezifischen Objekt bzw. Objekten.

---

## Daten
Eine Variable ist eine benannte Adressierung, in der man was speichern kann. Jede Variable besitzt einen Datentyp, dieser regelt die Speichergröße und das Verhalten.
### Arbeiten mit Zahlen
Grundsätzlich gibt es zwei Arten von Zahlen:
* Ganze Zahlen -> Integer Values
* Dezimalzahlen (Kommazahlen) -> Real Values

#### Integer values
C# enthält mehrere Datentypen, um ganze Zahlen speichern zu können. Sie unterscheiden sich in der Speichergröße und in der Art des Zahlenbereichs:

| Datentyp | Speichergröße | Zahlenbereich |
| ----- | ----- | ----- |
| sbyte | 8 bits |-128 to 127|
|byte |8 bits| 0 to 255 |
|short |16 bits |-32,768 to 32,767|
|ushort| 16 bits| 0 to 65,535|
|int| 32 bits |-2,147,483,648 to 2,147,483,647|
|uint |32 bits| 0 to 4,294,967,295|
|long| 64 bits| -9,223,372,036,854,775,808 to 9,223,372,036,854,775,807|
|ulong |64 bits |0 to 18,446,744,073,709,551,615|
|char |16 bits |0 to 65,535|

Aufpassen! Wenn man über das Zahlenlimit hinausgeht z.B. bei Byte 255 + 1 landet man wieder beim Anfang also 0.

#### Real values
Standard Datentypen zum Speichern von Dezimalzahlen sind *float* und *double*.
* float (32 bits)   
 Bereich: 1.5E-45 bis 3.4E48   
 7 Nachkommastellen
* double (64 bits)   
  Bereich: 5.0E-324 bis 1.7E308   
  15 Nachkommastellen

Es gibt aber noch den Datentyp *decimal*. Dieser hat einen bisschen niedrigeren Zahlenbereich als double, kann aber dafür 28-29 Nachkommastellen speichern.

Falls du float- oder double-Literale in deinem Code verwendest, weist der Compiler ob du float oder double meinst. Die Schreibweise entscheidet:  
Ein float: 2.5f   
Ein double: 2.5

### Text


---
### Book Completion --> 29 / 216

# Quellenverzeichnis
* Leitpfaden: C# Programming Yellow Book by Rob Miles <http://www.csharpcourse.com/>  
* Allgemein <https://de.wikipedia.org/wiki/C-Sharp>  
* Namespaces <https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/namespace>
