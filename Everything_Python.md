# Everything_Python
Python ist eine interpretierte objektorientierte Programmiersprache.
```python
print("Hallo Welt!")
```
## Print
Fangen wird mit dem wohl nützlichsten Befehl an: `print()`, dieser gibt einen übergebenen String aus. Seit Python3 werden die Klammern benötigt.

Falls man einen String ausgeben möchte, der über mehrere Zeilen geht, kann man drei Anführungszeichen verwenden.
```python
haiku = """The old pond,
A frog jumps in:
Plop!"""
print(haiku)
```

## Kommentare
In Python werden Zeilenkommentare mit `#` geschrieben. Kommentare die über mehrere Zeilen, sollen mit `""" <text> """` bzw `''' <text> '''` gemacht werden.

## Variablen
In Python muss man nicht den Datentyp einer Variable deklarieren, dieser wird dynamisch festgelegt.
```Python
# Bin ein String
city_name = "St. Potatosburg"
# Bin ein Integer
city_pop = 340000
```
Mit `type(variablenname)` kann der Datentyp ausgegeben werden
```python
print(type(city_pop))
>>> <type 'int'>
```
Variablen, die Boolean als Datentyp verwenden sollen, können mit *True* oder *False* initialisiert werden.
```python
a = True
b = False
```
Man kann auch mehrere Variablen in einer Zeile initialisieren, indem man Beistriche verwendet.
```python
a,b,c = 1,2,"john"
```
Falls man eine Variable benutzen will, wo ein anderer Datentyp wie z.B. String benötigt wird, dann kann man diese casten, im Fall von String mithilfe der `str()` Methode.
```python
age = 19
print "I am " + str(age) + " years old!"
>>> "I am 19 years old!"
```

## Arithmetische Operatoren
Nehmen wir eine Variable `a = 10` und `b = 20`

Operator | Beschreibung | Beispiel
--- | --- | ---
+ Addition | Adds values on either side of the operator. |	a + b = 30
- Subtraction	| Subtracts right hand operand from left hand operand. |	a – b = -10
* Multiplication |	Multiplies values on either side of the operator |	a * b = 200
/ Division |	Divides left hand operand by right hand operand	| b / a = 2
% Modulus |	Divides left hand operand by right hand operand and returns remainder	| b % a = 0
** Exponent |	Performs exponential (power) calculation on operators |	a**b =10 to the power 20
//	Floor Division | The division of operands where the result is the quotient in which the digits after the decimal point are removed. But if one of the operands is negative, the result is floored, i.e., rounded away from zero (towards negative infinity) |	9//2 = 4 and 9.0//2.0 = 4.0, -11//3 =


## Logische Operatoren
Nehmen wir eine Variable `a = 10` und `b = 20`

Operator | Beschreibung | Beispiel
--- | --- | ---
==	| If the values of two operands are equal, then the condition becomes true.	| (a == b) is not true.
!=	| If values of two operands are not equal, then condition becomes true.	| (a != b) is true.
<>	| If values of two operands are not equal, then condition becomes true.	| (a <> b) is true. This is similar to != operator.
>	| If the value of left operand is greater than the value of right operand, then condition becomes true.|	(a > b) is not true.
< |	If the value of left operand is less than the value of right operand, then condition becomes true.| 	(a < b) is true.
>= |	If the value of left operand is greater than or equal to the value of right operand, then condition becomes true.|	(a >= b) is not true.
<=|	If the value of left operand is less than or equal to the value of right operand, then condition becomes true.	|(a <= b) is true.

## Assignment Operatoren
Nehmen wir eine Variable `a = 10` und `b = 20`

Operator | Beschreibung | Beispiel
--- | --- | ---
=	| Assigns values from right side operands to left side operand|	c = a + b assigns value of a + b into c
+= |Add AND	It adds right operand to the left operand and assign the result to left operand	|c += a is equivalent to c = c + a
-=| Subtract AND	It subtracts right operand from the left operand and assign the result to left operand	|c -= a is equivalent to c = c - a
*= |Multiply AND	It multiplies right operand with the left operand and assign the result to left operand|	c *= a is equivalent to c = c * a
/= |Divide AND	It divides left operand with the right operand and assign the result to left operand|	c /= a is equivalent to c = c / ac /= a is equivalent to c = c / a
%= |Modulus AND	It takes modulus using two operands and assign the result to left operand|	c %= a is equivalent to c = c % a
**=| Exponent AND	Performs exponential (power) calculation on operators and assign value to the left operand	|c **= a is equivalent to c = c ** a
//=| Floor Division	It performs floor division on operators and assign value to the left operand	|c //= a is equivalent to c = c // a

***

## String
Falls man einfache oder normale Anführungszeichen in einem String verwenden möchte, muss man diese mit `\` escapen.
```python
'There\'s a snake in my boot!'
```
Jeder Character in einem String wird indexiert und kann über diesen aufgerufen werden.
```python
"""
The string "PYTHON" has six characters,
numbered 0 to 5, as shown below:

+---+---+---+---+---+---+
| P | Y | T | H | O | N |
+---+---+---+---+---+---+
  0   1   2   3   4   5

So if you wanted "Y", you could just type
"PYTHON"[1] (always start counting from 0!)
"""
fifth_letter = "MONTY"[4]

print(fifth_letter)
>>> Y
```
Nützliche Methoden in Bezug zu String:  
1 len() - Gibt die Anzahl der Character aus
```python
parrot = "Norwegian Blue"
print(len(parrot))
>>> 14
```
2 lower() - Wandelt alle Großbuchstaben zu Kleinbuchstaben um
```python
parrot = "Norwegian Blue"
print(parrot.lower())
>>> norwegian blue
```
3 upper() - Wandelt alle Kleinbuchstaben zu Großbuchstaben um
```python
parrot = "norwegian blue"
print(parrot.upper())
>>> NORWEGIAN BLUE
```
4 str() - Macht aus nicht-Strings Strings (Magic Method)
```python
"""Declare and assign your variable on line 4,
then call your method on line 5!"""
pi = 3.14
print(str(pi))
```
5 join() - Verbindet Elemente mit einem angegeben Zeichen
```python
s = "-";
seq = ("a", "b", "c"); # This is sequence of strings.
print s.join( seq )
>>> a-b-c
```
Strings können in mit + verbunden werden, bei der Print Methode kann man auch das gute allte Formatting mit dem %-Operator machen, wie in `printf()` in der Programmiersprache C.
```python
print("The value of pi is around " + str(3.14))
```
Vergleiche mit:
```python
pi = 3.14
print("The value of pi is around %f." % (pi))
```
Achtung es muss immer der passende %-Operator gewählt werden, für Float %f, für Integer %d, für String %s, ... .

## Entscheidungen (if - elif - else)
Einfache Entscheidungen können mit
```python
  if [Bedingung]:
    # Code
```
realisiert werden. Weiters kann man mit `else`, einen Fall definieren, wenn die Bedingung nicht eintritt. Man kann aber dazwischen noch mit `elif [Bedingung]:` weitere Zwischen-Bedingungen definieren, um weiter zu verschachteln.
```python
def greater_less_equal_5(answer):
    if answer > 5:
        return 1
    elif answer < 5:          
        return -1
    else:
        return 0

print(greater_less_equal_5(4))
print(greater_less_equal_5(5))
print(greater_less_equal_5(6))

>>> -1
>>> 0
>>> 1
```

## User-Input
User-Input, also Benutzereingaben per Command_line, können mit `input()` seit Python 3.0 realisiert werden. Ältere Versionen verwenden `raw_input()`.
```python
>>> eingabe = input("Ihre Eingabe? ")
Ihre Eingabe? 34
>>> eingabe
'34'
>>> eingabe = input("Ihre Eingabe? ")
Ihre Eingabe? [3,5,8]
>>> eingabe
'[3,5,8]'
>>> type(eingabe)
<class 'str'>
>>>
```

## Methoden
Grundsätzlich haben Methoden in Python folgenden Aufbau:
```python
def functionname( parameters ):
   "function_docstring"
   function_suite
   return [expression]
```
Mit dem Schlüsselwort **def** wird angegeben, dass eine Methode erstellt wird. Darauf folgt der Methodenname, der in Klammern die Parameter enthält. Grundsätzlich sollte unter der Kopfzeile die Dokumentation angegeben werden, auf die der Funktionsablauf der Methode folgt, die im Normalfall etwas zurückgibt mit dem Schlüsselwort **return**.
```python
def square(n):
  """Returns the square of a number."""
  squared = n ** 2
  print("%d squared is %d." % (n, squared))
  return squared

# Call the square function on line 10! Make sure to
# include the number 10 between the parentheses.

square(10)
>>> 10 squared is 100.
```
## Import
Was wenn man man Methoden wie `sqrt()` verwenden möchte? Diese Methode gehört dem Modul `Math` an und muss deshalb importiert werden. Module sind Grundsätzlich Dateien, die Python Definitionen und Anweisungen wie Beispielsweise Klassen, besitzen. Keine Sorgen, auf Klassen wird später noch tiefer eingegangen.

Möglichkeiten:  

* `import modul`  z.B. `import math` - importiert Math-Modul, somit kann man auf die Funktion `sqrt()` mit `math.sqrt()` zugreifen.
* `import modul as name` z.B. `import math as m` - importiert Math-Modul, aber mit dem Namen **m**, somit greift man auf die Funktion `sqrt()` mit `m.sqrt()` auf.
* `from module import name`  z.B. `from math import sqrt` - importiert nur `sqrt()`, in diesem Fall eine Methode. Diese kann jetzt einfach per `sqrt()` aufgerufen werden. Theoretisch könnte man beim Import-Statement ein **as** am Schluss einfügen, um den Namen der Methode zu verändern.
* `from module import *` Importiert, signalisiert durch den Stern, alles aus dem Modul. Dies ist zwar angenehm, den jetzt muss der Modulname nicht mehr beim Aufruf verwendet werden, es könnten aber Fehler, wie z.B. Überschreiben von bestehendem Objekten, dadurch entstehen.

## Loops
```python
while[Bedingung]:
  # Code
```
Führt den Code solange die Bedingung erfüllt, also True ist.
```python
loop_condition = True
while loop_condition:
  print()"I am a loop")
  loop_condition = False
```
Mit dem Schlüsselwort **break** wird der Schleifendurchgang beendet.
```python
count = 0
while True:
  print count
  count += 1
  if count >= 10:
    break
```
In Python weden **for**-Loops hauptsächlich zum Iterieren verwendet. Siehe dazu mehr bei **Lists**, **Dictionaries**. Man kann aber auch einen altbekannten **for**-Loop wie in Java `for(int i = 0; i < end_value; i++)` bilden: `for i in range(end_value):`
```python
print()"Counting...")
for i in range(2):
  print(i)
>>> Counting...
>>> 0
>>> 1
```
Wenn man über Strings iteriert, werden die einzelnen Zeichen immer geholt.
```python
thing = "spam!"
for c in thing:
  print c
>>> s
>>> p
>>> a
>>> m
>>> !
```
Man kann über mehrere Listen zugleich mit **for** iterieren mit Hilfe der Methode `zip()`.
```python
list_a = [3, 9, 17, 15, 19]
list_b = [2, 4, 8, 10, 30, 40, 50, 60, 70, 80, 90]

for a, b in zip(list_a, list_b):
  if a > b:
    print a
  else:
    print b
>>> 3
>>> 9
>>> 17
>>> 15
>>> 30
```

## Lists
Lists sind Datentypen die eine Vielzahl von Informationen unter einem einzelnen Variablennamen speichern.
```python
list_name = [item_1, item_2]
```
Die einzelnen Werte werden über den Index angesprochen mit `[index_nummer]`.
```python
numbers = [5, 6, 7, 8]
print(numbers[1] + numbers[3])
>>> 14
```
Lists haben keine fixe Länge, mit `append()` können neue Werte hinzugefügt werden.
```python
suitcase = []
suitcase.append("sunglasses")
# Your code here!
suitcase.append("a")
suitcase.append("b")
suitcase.append("c")

list_length = len(suitcase) # Set this to the length of suitcase

print "There are %d items in the suitcase." % (list_length)
print(suitcase)
>>> There are 4 items in the suitcase.
['sunglasses', 'a', 'b', 'c']
```
Es können auch Werte mithilfe von `insert()` zwischendurch hinzugefügt werden.
```python
animals = ["aardvark", "badger", "duck", "emu", "fennec fox"]

# Your code here!
duck_index = animals.index("duck") # Use index() to find "duck"
animals.insert(duck_index, "cobra")

print(animals) # Observe what prints after the insert operation
>>> ['aardvark', 'badger', 'cobra', 'duck', 'emu', 'fennec fox']
```
Mit `sort()` können Listen sortiert werden.
```python
start_list = [5, 3, 1, 2, 4]
start_list.sort()
print(start_list)
>>> [1, 2, 3, 4, 5]
```
Mit dem Zeichen **;** kann man eine Anzahl von Elemente in einer Liste holen, indem man einen Index-Bereich angibt (=Slicing Syntax).
```python
suitcase = ["sunglasses", "hat", "passport", "laptop", "suit", "shoes"]
# The first and second items (index zero and one)
first = suitcase[0:2]
# Third and fourth items (index two and three)
middle = suitcase[2:4]
# The last two items (index four and five)
last = suitcase[4:6]
```
Man kann auch links und rechts neben **:** nichts angegeben, damit werden vom Anfang bzw. bis zum Ende der Liste Werte geholt.
```python
animals = "catdogfrog"
# The first three characters of animals
cat = animals[:3]
# The fourth through sixth characters
dog = animals[3:6]
# From the seventh character to the end
frog = animals[6:]
```
Man kann mithilfe von **for**-Loops durch eine Liste iterieren.
```python
my_list = [1,9,3,8,5,7]

for number in my_list:
  # Prints number times 2
  print(2 * number)
```
`remove()`, `del` oder doch `pop()`?  
`remove()` entfernt den ersten Eintrag, der dem Wert entspricht.
```python
>>> a = [0, 2, 3, 2]
>>> a.remove(2)
>>> a
[0, 3, 2]
```
`del` entfernt einen Eintrag auf dem spezifischen Index.
```python
>>> a = [3, 2, 2, 1]
>>> del a[1]
>>> a
[3, 2, 1]
```
`pop()` entfernt das Element auf dem spezifischen Index und gibt diesen zurück.
```python
>>> a = [4, 3, 5]
>>> a.pop(1)
3
>>> a
[4, 5]
```
Man kann Listen dynamisch generieren. Dies wurde schon bei der **for**-Schleife angesprochen. Man kann aber dies weiter ausbauen und sogar **if**-Bedingungen einsetzen: `my_list = [my_variable for my_variable in range(start_value, end_value) if [Bedingung]]`
```python
even_squares = [x ** 2 for x in range(1, 12) if x % 2 == 0]
print(even_squares)
>>> [4, 16, 36, 64, 100]
```
Beim Slicing kann man einen weiteren Wert setzen, nämlich die Stride `[start:end:stride]` (Start ist included, also berücksichtig, End ist exclusive, wird nicht mehr berücksichtig). Diese gibt an, in welchen Abstand Werte genommen werden, bei einer Stride von 2 wird jeder zweite Wert der Liste genommen.
```python
l = [i ** 2 for i in range(1, 11)]
# Should be [1, 4, 9, 16, 25, 36, 49, 64, 81, 100]
print l[2:9:2]
>>> [9, 25, 49, 81]
```
Eine positive Stride geht in einer Liste von links nach rechts. Mit Angabe einer negativen Stride, wird die Liste von rechts nach links gegangen.
```python
to_one_hundred = range(101)
backwards_by_tens = to_one_hundred[::-10]
print backwards_by_tens
>>> [100, 90, 80, 70, 60, 50, 40, 30, 20, 10, 0]
```

## Dictionaries
Dictionaries sind ähnlich zu Listen, Elemente werden aber nicht mit einem Index, sondern über einen **key** geholt.
```python
d = {'key1' : 1, 'key2' : 2, 'key3' : 3}
```
```python
# Assigning a dictionary with three key-value pairs to residents:
residents = {'Puffin' : 104, 'Sloth' : 105, 'Burmese Python' : 106}

print(residents['Puffin']) # Prints Puffin's room number
>>> 104
```
Neue Werte können einfach mit `my_dict['key'] = some_value` erstellt werden.
```python
menu = {} # Empty dictionary
menu['Chicken Alfredo'] = 14.50 # Adding new key-value pair
print(menu['Chicken Alfredo'])
>>> 14.5
```
Elemente in einem Dictionary können mit dem Schlüsselwort **del** und dem entsprechenden **key** des Elements gelöscht werden. `del my_dict['key']`

Dictionaries können wie Listen mit **for** iteriert werden, dabei geht man aber durch die **Keys** und nicht die **Values**!
```python
# A simple dictionary
d = {"foo" : "bar"}

for key in d:
  print(d[key])  # prints "bar"
```

## Klassen
Python ist eine objektorientierte Sprache. Um Objekte zu instanziieren werden **Klassen** benötigt. Denn Python muss zur Laufzeit schauen, wenn eine Methode aufgerufen, ob diese in der entsprechenden Klasse erstellt wurde, um wenn ja diesen Code dann ausführen.  
Klassen werden mit dem Keyword **class** erstellt, auf den der Klassenname mit Klammern kommt, wo die **Vererbung** angegeben wird.
Jede Klasse benötigt eine `__init__()`-Funktion, die zur Initialisierung von Objekten dieser Klasse benötigt wird. `__init__()` benötigt immer mindestes einen Parameter: `self`. Diese referenziert das Objekt, was erstellt wird.
```python
# Define Animal-object, that contains a name as an argument
class Animal(object):
  def __init__(self, name):
    self.name = name
# Create object and print the attribute-name
zebra = Animal("Jeffrey")
print(zebra.name)
```
In Klassen werden Methoden definiert, die mit den Objekten dieser Klasse arbeiten.
```python
class Animal(object):
  """Makes cute animals."""
  is_alive = True
  health = "good"
  def __init__(self, name, age):
    self.name = name
    self.age = age
  # New method
  def description(self):
    print(self.name)
    print(self.age)

hippo = Animal("Franz", 2)
hippo.description()
>>> Franz
>>> 2
```
Vererbung in Python wird mittels Angabe der Superklasse in den Klammern der Parameter bei der Klasseerstellung realisiert.
In der `__init__()`-Methode sollte auch die gleichnamige Methode der Superklasse benutzt werden, alternativ kann man das Schlüsselwort `super()` verwenden. Man sollte aber konsequent in der Vererbungshierarchie entweder `Super_class_name.__init__()` oder `super().__init__()`, da sonst Fehler auftreten könnten.
```python
class Person:
    def __init__(self, vorname, nachname, geburtsdatum):
        self._vorname = vorname
        self._nachname = nachname
        self._geburtsdatum = geburtsdatum

    def __str__(self):
        ret = self._vorname + " " + self._nachname
        ret += ", " + self._geburtsdatum
        return  ret

class Angestellter(Person):
    def __init__(self, vorname, nachname, geburtsdatum, personalnummer):
        Person.__init__(self, vorname, nachname, geburtsdatum)
        # alternativ:
        #super().__init__(vorname, nachname, geburtsdatum)
        self.__personalnummer = personalnummer

    def __str__(self):
        #return super().__str__() + " " + self.__personalnummer
        return Person.__str__(self) + " " + self.__personalnummer

if __name__ == "__main__":
    x = Angestellter("Homer", "Simpson", "09.08.1969", "007")
    print(x)
>>> Homer Simpson, 09.08.1969 007
```
Methoden können in Python überschrieben werden, das heißt, dass eine Subklasse eine Methode der Superklasse neu definiert. Dies wird im vorherigen Beispiel bei der Methode `__str__()` beim Angestellten auch gemacht. Dagegen ist das Überladen von Methoden wie z.B. in Java nicht möglich. Überladen bedeutet, dass es eine oder mehrere Methoden mit gleichen Methodennamen gibt, aber anderer Signatur (andere Parametertypen, mehr Parameter, ...). Eigentlich wird dies in Python eigentlich nicht gebraucht, da man ja alle Datentypen als Parameter akzeptiert und dadurch im Code mit **if** einfache Bedingung zur Unterscheidung, mit anderen Code, machen kann.
```python
def method(x):
    if type(var) is str: #Check if x is a string
        print("I am a string!")
    if isinstance(var, str): #Another way to check if x is a string
        print("I am still a string!")
    if type(var) is int: #Check if x is an integer
        print("I am an integer!")
    if isinstance(var, int): #Another way to check if x is an integer
        print("I am still an integer!")

var = "baum"
method(var)
>>> I am a string!
>>> I am still a string!
```
Private Attribute und Methoden besitzen in Python zwei Unterstriche (`__`) vor ihrem Namen.
```python
class P:
   def __init__(self, name, alias):
      self.name = name       # public
      self.__alias = alias   # private

   def who(self):
      print('name  : ', self.name)
      print('alias : ', self.__alias)

x = P(name='Alex', alias='amen')
print(x.alias)
>>> AttributeError: P instance has no attribute 'alias'
```
Wirklich private ist aber nichts in Python, denn mit `_Klassename__` kann auf private Elemente zugegriffen werden. Python verfolgt  eine offene Philosophie.
```python
print(x._P__alias)
>>> 'amen'
```

## Magic Methods
Sind spezielle Methoden (bei Objekten), die Python "magisch" zur richtigen Zeit einsetzt. Wenn man ein neues Objekt erzeugt, ruft Python die `__init__()`-Methode auf, eine der "magischen Methoden". Diese beginnen und enden mit `__`. Mit ihnen könne auch Verhalten ausprogrammiert werden, z.B. wie reagiert ein Objekt auf den + Operator? Dies kann durch die Realisierung der Methode `__add__(self, other)` realisiert werden. Nachfolgend folgt eine Übersicht der meisten "Magic Methods"  
Achtung | [Pipe] Symbol wurde unten durch \\ ersetzt, wegen Parser-Fehler.

Binäre Operatoren

Operator | Methode
--- | ---
+	| object.\__add__(self, other)
-	| object.\__sub__(self, other)
*	| object.\__mul__(self, other)
//	| object.\__floordiv__(self, other)
/	| object.\__truediv__(self, other)
%	| object.\__mod__(self, other)
**	| object.\__pow__(self, other[, modulo])
<<	| object.\__lshift__(self, other)
>>	| object.\__rshift__(self, other)
&	| object.\__and__(self, other)
^	| object.\__xor__(self, other)
\\ |  object.\__or__(self, other)

Erweiterte Zuweisungen

Operator | Methode
--- | ---
+= |	object.\__iadd__(self, other)
-= |	object.\__isub__(self, other)
*= |	object.\__imul__(self, other)
/= |	object.\__idiv__(self, other)
//=	 |object.\__ifloordiv__(self, other)
%= |	object.\__imod__(self, other)
**= |	object.\__ipow__(self, other[, modulo])
<<= |	object.\__ilshift__(self, other)
>>= |	object.\__irshift__(self, other)
&= |	object.\__iand__(self, other)
^= |	object.\__ixor__(self, other)
\\=  | 	object.\__ior__(self, other)

Unäre Operatoren

Operator | Methode
--- | ---
-	| object.\__neg__(self)
+	| object.\__pos__(self)
abs()	| object.\__abs__(self)
~	| object.\__invert__(self)
complex()	| object.\__complex__(self)
int()	| object.\__int__(self)
long()	| object.\__long__(self)
float()	| object.\__float__(self)
oct() |	object.\__oct__(self)
hex() |	object.\__hex__(self

Vergleichsoperatoren

Operator | Methode
--- | ---
<	| object.\__lt__(self, other)
<= | 	object.\__le__(self, other)
==	| object.\__eq__(self, other)
!=| 	object.\__ne__(self, other)
>=| 	object.\__ge__(self, other)
>	| object.\__gt__(self, other)

## Input/Output
Mit der Methode `open()` können Dateien geöffnet werden, ihr Name wird als String als erster Parameter übergeben. Als zweiter Parameter wird der Modus angegeben:
* "w" - Schreib-Modus (write-only mode)
* "r" - Lese-Modus (read-only mode)
* "r+" - Schreib- und Lese-Modus (read and write mode)
* "a" - Fügt am Ende der Datei neu Informationen ein (append mode)

In die "geöffneten" Dateien kann mit `write()` Information geschrieben werden. Mit `read()` können Dateien eingelesen werden. Mit `readline()`wird nur eine einzelne Zeile der Datei gelesen. Nach allen Vorgängen sollte unbedingt `close()` aufgerufen werden, damit alles sauber geschlossen wird.
```python
# Reads and prints the file output.txt
my_file = open("output.txt", "r")
print(my_file.read())
my_file.close()
```
Python besitzt spezielle "Events", die die `__enter__()` und `__exit__()` Methoden ausführen, die für Input/Output das Öffnen und Schließen realisieren, damit man es nicht selber tut. Dies kann mit den Schlüsselwörtern **with** und **as** realisiert werden:
```python
with open("file", "mode") as variable:
  # Read or write to the file
  # Like variable.read() or variable.write("Bla") and so on
```
## Exception Handling
In Python erfolgt das Exception-Handling ähnlich wie in Java. Mit `try:` wird ein Block erstellt, in dem kritische Operationen stehen. Mittels `except [(optional) Exception_name]:` werden diese abgefangen. Es können mehrere `except`-Blöcke definiert werden, falls spezifische Exception abgefangen werden sollen, muss der Name angegeben werden, falls alle abgefangen werden sollen keiner. Am Schluss kann man einen `finally:` Block realisieren, der egal ob eine Exception aufgetreten ist oder nicht, ausgeführt wird.
```python
try:
  f = open("demofile.txt")
  f.write("Lorum Ipsum")
except:
  print("Something went wrong when writing to the file")
finally:
  f.close()
```

## Lambda
Mit dem Schlüsselwort **Lambda** können "ein-Mal-Benutzen"-Methoden definiert werden. Dadurch muss man Methoden nicht definieren, wenn man einen Algorithmus nur einmal benutzen muss.
```python
lambda x: x % 3 == 0
```
Entspricht der Methode:
```python
def by_three(x):
  return x % 3 == 0
```
Beispiel, aus einer Liste sollen alle Werte gefiltert werden, die durch 3 teilbar sind.
```python
my_list = range(16)
print(filter(lambda x: x % 3 == 0, my_list))
>>> [0, 3, 6, 9, 12, 15]
```

# Quellen
https://www.tutorialspoint.com/python/python_quick_guide.htm

https://www.stavros.io/tutorials/python/

https://www.python-kurs.eu/python3_eingabe.php

https://wiki.pythonde.pysv.org/Import

https://www.python-kurs.eu/python3_vererbung.php

https://www.codecademy.com/learn

https://www.bogotobogo.com/python/python_private_attributes_methods.php
