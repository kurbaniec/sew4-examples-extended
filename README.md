# SEW4 Examples

Dieses Repository basiert auf den Repository https://github.com/TGM-HIT/sew4-examples von M. Borko und dient zur besseren Erklärung von diesem.

# Quellen
* http://www.sphinx-doc.org/en/stable/ext/autodoc.html#module-sphinx.ext.autodoc
* http://www.sphinx-doc.org/en/stable/domains.html#python-signatures
* https://samnicholls.net/2016/06/15/how-to-sphinx-readthedocs/
* nice example: http://voevent-parse.readthedocs.io/en/stable/
* nice theme: https://github.com/rtfd/sphinx_rtd_theme

