from multiprocessing import Process, Value, Array
import ctypes

class WorkerProcess(Process):
    """
    Stellt einen simplen Worker-Prozess dar,
    welcher einen geteilten Wert speichert und ein
    geteiltes Array verändert.
    """
    def __init__(self, v, arr):
        """
        Initialisiert die Basisklasse Process
        :param v: ein geteilter Wert
        :param arr: ein geteiltes Array
        """
        Process.__init__(self)
        self.v = v
        self.arr = arr

    def run(self):
        """
        Ändert den Wert und das Array
        :return: None
        """

        """
        with bewirkt, dass Locks angewendet werden
        Diese sind wichtig, da ohne sie jeder Prozess willkürlich auf die Variablen zugreifen kann
        Dabei kann passieren, dass ein Prozess und ein anderer gleichzeitig auf die Variable +2 speichern. Die beiden
        Prozesse achten nicht gegenseit aufeinander und arbeiten mit den gleichen Ursprungswert anstatt auf den neuen zu 
        warten.
        
        mit Lock
        value = 0
        P1 -> holen von value = 0 -> += 2 -> value = 2
        P2 wartet bis P1 beendet-> -> holen von value = 2 -> erst dann += 2 -> value = 4
        
        ohne Lock
        value = 0
        P1 -> holen von value = 0 -> += 2 -> value = 2
        P2 wartet nicht auf P1 -> holen von value = 0 -> += 2 -> value = 2
        """
        with self.v.get_lock():
            self.v.value += 2
        with self.arr:
            for i in range(len(self.arr)):
                self.arr[i] = -self.arr[i]


if __name__ == "__main__":
    # Erstellt eine Shared-Memory-Value vom C-Typ Double mit dem Wert 0.0
    v = Value('d', 0.0)
    # Gleichwertig: v = Value(ctypes.c_double, 0.0)
    # Erstellt eine Shared-Memory-Array vom C-Typ Integer mit der Größe 10, mit den Werten von 0 bis 9
    arr = Array('i', range(10))
    # Gleichwertig: arr = Array(ctypes.c_int, range(10))
    workers = []
    for i in range(800):
        # Erstellt einen neuen Workerprozess
        w = WorkerProcess(v, arr)
        # Fügt ihn ein bi
        workers.append(w)
        w.start()

    for w in workers:
        w.join()

    print(v.value)
    print(arr[:])
