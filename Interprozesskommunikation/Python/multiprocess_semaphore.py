from multiprocessing import Process, Semaphore
import os

def writefile(semaphore, num):
    """
    Schreibt Nachrichten nacheinander in getrennte Files
    und verwendet dabei einen Semaphor, damit nicht zu viele
    Prozesse gleichzeitig das Filesystem belasten.
    :param semaphore: der zu verwendende Semaphor
    :param num: die Prozessnummer
    :return: None
    """
    # Mitteilen, dass man auf Semaphor-Freigabe wartet
    print("Process "+str(num)+": Waiting for semaphore...")
    # Semaphor, der zwei Prozesse zulässt
    with semaphore:
        # Mitteilen, dass man einer der beiden Prozesse ist
        # die gerade auf kritischen Teil zugreifen
        print("Process "+str(num)+": Acquired semaphore!")
        # Bestimmtes File öffnen
        with open("foo"+str(num)+".txt", "ab") as fo:
            # Viele Zeilen schreiben
            for i in range(500000):
                fo.write("Process ".encode())
                fo.write(str(num).encode())
                fo.write(": Hello!\n".encode())
    # Mitteilen, dass man fertig ist
    print("Process "+str(num)+": Released semaphore!")

if __name__ == "__main__":
    # Files löschen, falls sie bereits existieren
    for num in range(10):
        if os.path.isfile("foo"+str(num)+".txt"):
            os.remove("foo"+str(num)+".txt")
    # Zwei Prozesse dürfen gleichzeitig schreiben
    sem = Semaphore(value=2)
    # Zehn Prozesse erstellen und starten
    for num in range(10):
        Process(target=writefile, args=(sem, num)).start()